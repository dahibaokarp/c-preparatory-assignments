/*An inventory management system needs to manage data of the items into binary file. Each item has id, name, price and quantity. Write a menu-driven program to implement add, find, display all, edit and delete operations from the items file. Order id (int) should be generated automatically and must be unique.
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//Create struct item
typedef struct item
{
    int id;
    char name[50];
    double price;
    int quantity;   
}item_t;

void delete_item()
{
    int id, found=0;
    item_t i;
    printf("Enter item ID : "); //accept id
    scanf("%d",&id);    
    FILE *fp;//create file
    FILE *fp_tmp;//create temp file
    fp=fopen("item.db","rb"); //open file and temp file
    fp_tmp=fopen("temp.db","wb");
     if(fp==NULL)//NULL if not able to open file
    {
        printf("failed to open items file");
        return;
    }

     if(fp_tmp==NULL)
    {
        printf("failed to open temp file");
        return;
    }
    while(fread(&i,sizeof(item_t),1,fp)>0)//read file
    {
        if(id==i.id)
        {
            found = 1;
            printf("Item deleted from inventory",i.id);   
        }
        else
        {
            fwrite(&i, sizeof(item_t),1,fp_tmp);
        }
        
    }
    if(!found)
    {
        printf("\n No such item found \n");

    }
            
        
    fclose(fp);// close both files
    fclose(fp_tmp);

    remove("item.db");//delete file and temp file
    rename("temp.db","item.db");
}

void edit_item()
{
	FILE *fp;
	item_t i;
	int id;
	int found=0;
	printf("Enter item ID :");
    scanf("%d",&id); 
	fp = fopen("item.db", "rb+");
	if(fp == NULL) 
    {
	printf("cannot open user file");
		return;
	}
	while(fread(&i, sizeof(item_t), 1, fp) > 0) 
	{
		if (id==i.id)
		{
			found=1;
	        break;
		}
		
	}
		if (found)
		{		
		printf("\nEnter the new Item Name :");
		scanf("%s",&i.name);
		printf("\nEnter the new Item Price :");
		scanf("%lf",&i.price);
		printf("\nEnter quantity of Item :");
		scanf("%d",&i.quantity);
		fseek(fp,sizeof(item_t), SEEK_CUR);
		fwrite(&i, sizeof(item_t), 1, fp);
		printf("Item updated.\n");
		}
		else
		{
			printf("\n No such item found");
		}
		
	fclose(fp);
}

void display_all_item()
{
    FILE *fp;
    item_t i;
    
    fp=fopen("item.db","rb");
     if(fp==NULL)
    {
        printf("failed to open item file");
        return;
    }
    while (fread(&i,sizeof(item_t),1,fp)>0)
    {        
        printf("\n id : %d  name : %s  price : %.2lf  quantity : %d ", i.id, i.name,i.price,i.quantity);    
    }
    fclose(fp);
}

void find_item(char name[])
{
	FILE *fp;
	int found =0;
	item_t a;
	fp = fopen("item.db","rb");
	if(fp==NULL) {
		printf("failed to open books file.");
		return;
	}
	while(fread(&a, sizeof(item_t),1,fp) >0) 
	{
		if(strstr(a.name,name) !=NULL)
		{
			found = 1;
			printf("\n id : %d  name : %s  price : %.2lf  quantity : %d ", a.id, a.name, a.price,a.quantity);
		}
	}
	fclose(fp);
	if(!found)
		printf("No such Item found.\n");
}


int get_next_id() 
{
	FILE *fp;
	int max=0;
	int size=sizeof(item_t);
	item_t a;
	fp=fopen("item.db", "rb");
	if(fp==NULL)
		return max+1;
	fseek(fp,-size,SEEK_END);
	if(fread(&a,size,1,fp)>0)
		max=a.id;
	fclose(fp);
	return max+1;
}


void add_item()
{
item_t a;
a.id=get_next_id();
//scanf("%d",&a.id);
printf("Enter Name Of Item:");
scanf("%s",a.name);
printf("Enter Price Of Item:");
scanf("%lf",&a.price);
printf("Enter Quantity Of item:");
scanf("%d",&a.quantity);
FILE *fp;
	fp=fopen("item.db", "ab");
	if(fp==NULL) 
    {
		printf("failed to open item file");
		return;
	}
	
	fwrite(&a, sizeof(item_t), 1, fp);
    
	printf("Item added into file.\n");
	
	fclose(fp);
}


int main()
{    
    int choice;
    char name[30];
 do
 { 
    printf("\n1.add item\n2.find item\n3.display all items\n4.edit item\n5.delete item\nEnter your choice:");
    scanf("%d",&choice);
    switch (choice)
    {
    case 1:
        add_item();
        break;
    case 2:
    printf("Enter item to be searched: ");
    scanf("%s",name);
        find_item(name);
        break;
    case 3:
       display_all_item();
        break;
    case 4:
        edit_item();
        break;
    case 5:
       delete_item();
        break;
    default:
     printf("Enter valid choice");
        break;
    }
 }while (choice!=0);
 return 0;
}