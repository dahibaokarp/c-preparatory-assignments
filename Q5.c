 /*Q5. Input employee information from the user including his
   employee id, name, address, salary, birth date and date of joining.
   Find the age of person when he joined company (in years) and his experience
   till date (in months). Also print the date when his probation period is over,
   assuming that probation period is of 90 days from date of joining
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

    struct date
    { int day,month,year;};

	struct employee
	{ char name [50];
	  int id;
	  char address[50];
	  int salary;
	  struct date birth;
	  struct date joining;
	};

	int main()
	{ int age,join,birth,exp,a,b,c,exp1,exp2,y,m,d;

	  struct employee e1;
	  printf("enter person info:\n   ");
	  setvbuf(stdout,NULL,_IONBF,0);
	  printf("name\n");
	  scanf("%s",e1.name);
	  printf("id\n");
	  scanf("%d",&e1.id);
	  printf("address\n");
	  scanf("%s",e1.address);
	  printf("salary\n");
	  scanf("%d",&e1.salary);
	  printf("date of birth(DD/MM/YYYY)\n");
	  scanf("%d %d %d",&e1.birth.day,&e1.birth.month,&e1.birth.year);
	  printf("date of joining(DD/MM/YYYY)\n");
	  scanf("%d %d %d",&e1.joining.day,&e1.joining.month,&e1.joining.year);


//	  scanf("%s %d %s %d %d %d %d %d %d %d" ,e1.name,&e1.id,e1.address,&e1.salary,&e1.birth.day,&e1.birth.month,&e1.birth.year);
	  printf ("name= %s \n employee id= %d \n address= %s \n salary= %d \n date of birth= %d-%d-%d \n date of joining= %d-%d-%d",e1.name,e1.id,e1.address,e1.salary,e1.birth.day,e1.birth.month,e1.birth.year,e1.joining.day,e1.joining.month,e1.joining.year);
	  birth=e1.birth.year;
	  join= e1.joining.year;
	  age= join-birth;
	  printf("\n age= %d",age);
	  printf("\n enter today's date(DD/MM/YYYY");
	  scanf("%d %d %d",&a,&b,&c);
	  printf("Today date- %d-%d-%d",a,b,c);
	  exp= c-join;
	  exp1=exp*12 - e1.joining.month;
	  exp2=exp1 + b;
	  printf("\nexperience= %d",exp2);

	y=e1.joining.year;
    m=e1.joining.month;
    d=e1.joining.day ;
    struct tm t = { .tm_year=y-1900, .tm_mon=m-1, .tm_mday=d };
    /* modify */
    t.tm_mday += 90;
    mktime(&t);
    /* show result */
    printf("\n%s", asctime(&t));
	  return 0 ;}

