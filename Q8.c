#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MOVIE_DB "movies.csv"
//Created Structure of Movie
typedef struct movie
{   
    int id;
    char name[100];
    char genres[300];
}movie_t;

//Displayed Movies content
void movie_display(movie_t *m)
{
    printf("id=%d, name=%s, genres=%s\n", m->id,m->name,m->genres);
}

//Create Node Structure
typedef struct node
{
    movie_t data;
    struct node *next;
}node_t;

node_t *head = NULL;//assign head to NULL

//Create New Node
node_t* create_node(movie_t val)
{
    node_t *newnode = (node_t*)malloc(sizeof(node_t));// void *malloc(size_t size) allocates memory and returns pointer to it.
    newnode->data=val;
    newnode->next=NULL;
    return newnode;

}

void add_last(movie_t val) //add last struct movie_ data
{
    node_t *newnode = create_node(val);
    if(head==NULL)
        head=newnode;
    else{
        node_t *trav =head;
        while(trav->next!=NULL)
        {
            trav=trav->next;
        }
        trav->next=newnode;

    }
}

void display_list()
{
    node_t *trav = head;
    while(trav!=NULL)
    {
        movie_display(&trav->data);
        trav=trav->next;
    }
}

int parse_movie(char line[],movie_t *m)
{   /*Split movie Content 
Line by Line by using strtok 
function and using "," delimiter*/
    int success=0;
    char *id,*name,*genres;   
    id=strtok(line, ",\n");
    name=strtok(NULL, ",\n");
    genres=strtok(NULL, ",\n");
    if(id == NULL || name == NULL || genres == NULL)//if there are no values return success=0
        success = 0;
    else
    {
        success = 1; //else 1
        m->id   = atoi(id); //atoi is library function converts string argument to integer type
        strcpy(m->name, name);//checks values entered to which are already there
        strcpy(m->genres,genres);

    }
    return success;

}

void load_movies()
{
    FILE *fp;   //create file
    char line[1024];
    movie_t m;      //declared struct variable
    fp=fopen(MOVIE_DB,"r");  //open file movies.csv in read mode
    if(fp==NULL)             //if not found return error
    {
        perror("failed to open movies file");
        exit(1);
    }

    while(fgets(line,sizeof(line),fp)!=NULL)
    {//read file line by line into fp
      //  printf("%s\n",line);
      parse_movie(line,&m);
      //movie_display(&m);
      add_last(m);
    }
    fclose(fp);
    
}

void find_movie_by_name()
{
    char name[100];
    node_t *trav = head; // trav points to head
    int found=0;
    printf("Enter Movie Name to be searched: ");
    gets(name);//reads line from stsin and into string i.e char pointed by name

    trav=head;
    while(trav!=NULL)
    {
        if(strcmp(name,trav->data.name)==0)//trav pointer name with name already in csv file
        {
            movie_display(&trav->data);//display node data
            found=1;
            break;
        }trav=trav->next;
   }
   if(!found)
        printf("movie name not found.\n");
}

void find_movie_by_genre()
{
    char genre[100];
    node_t *trav = head;
    int found=0;
    printf("Enter Movie genre to be searched: ");
    gets(genre);

    trav=head;
    while(trav!=NULL)
    {
        if(strstr(trav->data.genres,genre)!= NULL)// compares needle genre with haystack genres
        {
            movie_display(&trav->data);
            found=1;
            
        }trav=trav->next;
   }
   if(!found)
        printf("movie genre not found.\n");
}



int main()
{
    load_movies();
    display_list();
    find_movie_by_name();
    find_movie_by_genre();
    return 0;
}

//gcc Q8.c -o Q8.exe